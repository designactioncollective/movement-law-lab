<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
            if (is_home()) {
                if ($home = get_option('page_for_posts', true)) {
                    return get_the_title($home);
                }
                return __('Latest Posts', 'sage');
            }
            if ( is_category() ) {
               return single_cat_title( '', false );
            }
            if ( is_tag() ) {
                return single_tag_title( '', false );
            }
            if ( is_author() ) {
                return '<span class="vcard">' . get_the_author() . '</span>';
            }
            if ( is_tax('tribe_events_cat') ) {
                return single_term_title( '', false );
            }
            if ( is_post_type_archive('tribe_events') ) {
                return tribe_get_events_title();
            }
            if (is_singular('tribe_events')) {
                global $wp_query;
                return get_the_title($wp_query->queried_object->ID);
            }
            if ( is_post_type_archive() ) {
                return post_type_archive_title( '', false );
            }
            if ( is_tax() ) {
                return single_term_title( '', false );
            }
            if (is_archive()) {
                return get_the_archive_title();
            }
            if (is_archive('position')) {
                return __('Jobs & Internships', 'sage');
            }
            if (is_search()) {
                if ('' === get_search_query()) {
                    return __('Search', 'yli');
                } else {
                    return sprintf(__('Search Results for &ldquo;%s&rdquo;', 'yli'), get_search_query());
                }
            }
            if (is_404()) {
                return __('Not Found', 'sage');
            }
            return get_the_title();
        }
    }
