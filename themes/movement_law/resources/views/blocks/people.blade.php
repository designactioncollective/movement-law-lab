{{--
  Title: People
  Description: A grid of thumbnails and titles to display people
  Category: formatting
  Icon: groups
  Keywords: thumbnail people image
  Mode: edit
  Align: left
  PostTypes: page post
  SupportsAlign: left right
  SupportsMode: false
  SupportsMultiple: false
--}}

<blockquote data-{{ $block['id'] }} class="{{ $block['classes'] }}">


@if( have_rows('person_thumbnail') )
<div class="row">
  @while ( have_rows('person_thumbnail') )
  @php
    the_row();
      $image = get_sub_field('photo');
      $first = get_sub_field('first_name');
      $last = get_sub_field('last_name');
      $position = get_sub_field('position');
      $num = get_field('col_number');
      $colnumber = 12/$num;

  @endphp

  <div class="col-{!! $colnumber !!} mb-20">
    @if( !empty( $image ) )
      <img src="<?php echo esc_url($image['url']); ?> " alt="<?php echo esc_attr($image['alt']); ?>" />
    @endif

    <div class="name">@if($first) {!! $first !!} @endif
    @if($last) {!! $last !!} @endif
    </div>
    <div class="position">
    @if($position) {!! $position !!} @endif
    </div>
  </div> <!-- /col -->


  @endwhile
</div>
@else

@endif


</blockquote>

<style type="text/css">
  [data-{{$block['id']}}] {
  }
</style>
