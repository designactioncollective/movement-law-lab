{{--
  Template Name: Approach Page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
    @if( have_rows('approaches') )
    <div class="row">
      @while ( have_rows('approaches') )
        @php
          the_row();
          $image = get_sub_field('approach_image');
          $title = get_sub_field('approach_title');
          $text = get_sub_field('approach_text');
        @endphp
      <div class="col-3">
      @if( !empty( $image ) )
        <img src="<?php echo esc_url($image['url']); ?> " alt="<?php echo esc_attr($image['alt']); ?>" />
      @endif
      @if($title) <h5>{!! $title !!}</h5> @endif
      </div>
      @endwhile
    </div>

      @while ( have_rows('approaches') )
        @php
          the_row();
          $image = get_sub_field('approach_image');
          $title = get_sub_field('approach_title');
          $text = get_sub_field('approach_text');
        @endphp

      <div class="row">
        <div class="col image mb-20">
          @if( !empty( $image ) )
            <img src="<?php echo esc_url($image['url']); ?> " alt="<?php echo esc_attr($image['alt']); ?>" />
          @endif
        </div>
        <div class="col text">
          @if($title) <h3>{!! $title !!}</h3> @endif
          @if($text) {!! $text !!} @endif
        </div>
      </div>


      @endwhile
    @else

    @endif

  @endwhile
@endsection
