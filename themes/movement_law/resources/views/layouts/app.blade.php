<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    @if (is_front_page())
      <div class="wrap" role="document">
        <div class="content">
            @include('partials.home.home-section1')
            @include('partials.home.home-section2')
            @include('partials.home.home-section3')
            @include('partials.home.home-section4')
            @include('partials.home.home-section5')

        </div>
      </div>
    @else
    <div class="wrap" role="document">
      <div class="container">
      <div class="content row">
          <main class="main col">
            <div><?php dac_the_breadcrumbs('primary_navigation'); ?></div>
            @yield('content')
          </main>
        @if (App\display_sidebar())
          <aside class="sidebar col-3">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
  </div>
    @endif
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
