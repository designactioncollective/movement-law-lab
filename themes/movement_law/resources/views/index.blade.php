@extends('layouts.app')

@section('content')
  @include('partials.page-header')
@if (is_post_type_archive('resource'))
@php
echo do_shortcode('[searchandfilter id="222"]')
@endphp
@endif
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @if(is_post_type_archive('resource'))
    <div class="row">
  @endif

  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile
  @if(is_post_type_archive('resource'))
    </div>
  @endif
  {!! get_the_posts_navigation() !!}
@endsection
