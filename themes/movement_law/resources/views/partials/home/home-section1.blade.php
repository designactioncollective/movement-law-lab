<section class="home-1 text-center">
	<div class="container">
		<div class="row">
  		<div class="col">
  			<h2 class="h2-lg h4 display-4">home</h2>
				<?php
				// check if the repeater field has rows of data
					$id = 147;

					if( have_rows('slideshow', $id) ):
						$return = '<div class="owl-carousel">';
						// loop through the rows of data
						while ( have_rows('slideshow', $id) ) : the_row();
							// get all the subfields for each slide
							$return .= '<div>';
							// Link
							$link = get_sub_field('link');
							if ($link != '') $return .= '<a href="'.$link.'">';
							$image = get_sub_field('image');
							if( !empty($image) ):

								// vars
								$alt = $image['alt'];

								// thumbnail
								$size = 'slideshow';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];;
								$height = $image['sizes'][ $size . '-height' ];

								$return .= '<img src="'.$thumb.'" alt="'.$alt.'" width="'.$width.'" height="'.$height.'" />';

							endif;
							$return .= '<div class="slide-caption">';
							if (get_sub_field('caption')) $return .= '<h3>'.get_sub_field('caption').'</h3>';
							if (get_sub_field('description')) $return .= '<h4>'.get_sub_field('description').'</h4>';
							$return .= '</div>';
							if ($link != '') $return .= '</a>';
							$return .= '</div>';
						endwhile;
						$return .= '</div>';
					else :
						echo 'no rows found';
						$return .= 'No slides found.';
					endif;
					echo $return;

				?>
  		</div>
		</div>
	</div>
</section>
