<section class="home-2 mb-0 mb-sm-5">
	<div class="container">
		<div class="row">
		<div class="col-12"><h3 class="h2 text-center my-4"><a class="c-black" href="<?php site_url(); ?>/our-approach">Our Work</a></h3></div>
		</div>
		@if( have_rows('our_work') )
	    <div class="row">
	      @while ( have_rows('our_work') )
	        @php
	          the_row();
	          $image = get_sub_field('work_image');
	          $title = get_sub_field('work_title');
						$text = get_sub_field('work_text');
						$trim = mb_strimwidth($text, 0, 64, '');
	        @endphp

			      <div class="col-3">
			      @if( !empty( $image ) )
			        <img src="<?php echo esc_url($image['url']); ?> " alt="<?php echo esc_attr($image['alt']); ?>" />
			      @endif
			      @if($title) <h5>{!! $title !!}</h5> @endif
						@if($text) <div>{!! $trim !!}	</div> @endif
			      </div>

	      @endwhile
	    </div>
		@endif
	</div>
</section>
