<article <?php post_class(); ?>>
  <div class="row home-resource">
    <div class="col-12 image-wrap">
      <?php if(has_post_thumbnail()) : ?>
        <div class="embed-responsive embed-responsive-11by16">
          <a class="home-resource-image fadeIn-trigger image-fit_wrap" href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail(); ?>
          </a>
        </div>
      <?php endif; ?>
      <div class="post-text p-3">
        <h6 class="mb-0"><a class="" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
        <?php get_template_part('views/partials/_terms'); ?>
      </div><!-- end content col -->
    </div><!-- end imge -->

  </div>
</article>
