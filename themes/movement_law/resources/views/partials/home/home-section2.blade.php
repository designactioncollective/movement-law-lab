<section class="home-2 mb-0 mb-sm-5">
	<div class="container">
		<div class="row">
		<div class="col-12"><h3 class="h2 text-center my-4"><a class="c-black" href="<?php site_url(); ?>/our-approach">Our Approach</a></h3></div>
		</div>
		@if( have_rows('approaches', 25) )
	    <div class="row">
	      @while ( have_rows('approaches', 25) )
	        @php
	          the_row();
	          $image = get_sub_field('approach_image');
	          $title = get_sub_field('approach_title');
						$text = get_sub_field('approach_text');
						$trim = mb_strimwidth($text, 0, 64, '');
	        @endphp
					@if(get_sub_field('featured') == 1)
			      <div class="col-3">
			      @if( !empty( $image ) )
			        <img src="<?php echo esc_url($image['url']); ?> " alt="<?php echo esc_attr($image['alt']); ?>" />
			      @endif
			      @if($title) <h5>{!! $title !!}</h5> @endif
						@if($text) <div>{!! $trim !!}	</div> @endif
			      </div>
					@endif
	      @endwhile
	    </div>
		@endif
		<div class="row">
			<div class="col-12 view-all"><?php echo'<a class="btn btn-primary btn-arrow" href="'. site_url().'/our-approach">Learn More</a>'?></div>
		</div>
	</div>
</section>
