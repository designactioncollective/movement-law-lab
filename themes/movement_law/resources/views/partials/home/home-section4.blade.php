<section class="home-3 mb-0 mb-sm-5 bg-white bg-sm-greyXLight">
	<div class="container">
		<div class="row">
			<div class="col-12"><h3 class="h2 text-center my-4"><a class="c-black" href="<?php site_url(); ?>/resource">Resources & Tools</a></h3></div>
			<div class="col-12 home-resources">
					<?php
						$args = array(
							'post_type' => 'resource',
							'posts_per_page' => 3
						);
					$the_query = new WP_Query( $args ); ?>
					<?php if ( $the_query->have_posts() ) : ?>
					  <div class="row">
					  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					    <div class="col-sm  pb-4">
					    <?php get_template_part('views/partials/home/content-resource'); ?>
					    </div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					  </div>
					<?php endif; ?>
			</div>
			<div class="col-12 view-all"><?php echo'<a class="btn btn-primary btn-arrow" href="'. site_url().'/resource">All Resources</a>'?></div>
		</div>
	</div>
</section>
<!-- end home 3 -->
