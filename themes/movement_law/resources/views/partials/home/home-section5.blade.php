<section class="home-5 mb-0 mb-sm-5">
	<div class="container">
		<div class="row">
		<div class="col-12"><h3 class="h2 text-center my-4"><a class="c-black" href="<?php site_url(); ?>/our-approach">Get Involved</a></h3></div>
		</div>
		@if( have_rows('get_involved_links') )
	    <div class="row">
	      @while ( have_rows('get_involved_links') )
	        @php
	          the_row();
	          $link = get_sub_field('get_link');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';

	        @endphp

			      <div class="col-4">
							<a class="button btn btn-primary" href="<?php echo $link_url; ?>" target="<?php echo $link_target; ?>"><?php echo $link_title; ?></a>
			      </div>

	      @endwhile
	    </div>
		@endif
	</div>
</section>
<!-- end home 5 -->
