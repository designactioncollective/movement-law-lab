<footer class="content-info">
  <div class="container">
    <div class="row">
    <nav class="nav-footer">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'depth' => 1]) !!}
      @endif
    </nav>
  </div>
  <div class="row">
    <div class="col">
      <a class="footer-brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
        {!! wp_nav_menu(['menu' => 'footer_menu', 'menu_class' => 'nav', 'depth' => 0]) !!}
    </div>
    <div class="col">
        @include('partials/inserts._social')
    </div>
  </div>

</footer>
