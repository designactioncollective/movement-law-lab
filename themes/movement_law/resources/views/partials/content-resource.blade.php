
  <article @php post_class('col-6') @endphp>
  <?php if(has_post_thumbnail()) : ?>
      <a href="<?php echo get_the_permalink(); ?>">
        <?php the_post_thumbnail(); ?>
      </a>
  <?php endif; ?>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    @include('partials/_terms')
  </header>
</article>
