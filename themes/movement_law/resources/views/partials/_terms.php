<div class="terms d-none d-md-block fontSize-8 mt-2">
	<?php
	if(get_the_term_list($post->ID, 'resource_type')):
		echo '<ul class="type">'.get_the_term_list($post->ID, 'resource_type','<li class="">',  ', ', '</li>').'</ul>';
	endif;
	if(get_the_term_list($post->ID, 'subject')):
		echo '<ul class="type">'.get_the_term_list($post->ID, 'subject','<li class="">',  ', ', '</li>').'</ul>';
	endif;
	?>
</div>
