<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/


/*
 * Load all mu-plugins
*/

/* Functions */
require_once(dirname(__FILE__) . '/dac/functions/sidebar-menus.php');
require_once(dirname(__FILE__) . '/dac/functions/menu-shortcode.php');
require_once(dirname(__FILE__) . '/dac/functions/acf_share_options.php');
require_once(dirname(__FILE__) . '/dac/functions/pagination.php');
require_once(dirname(__FILE__) . '/dac/functions/site_links.php');

/* Custom post types and taxonomies */
require_once(dirname(__FILE__) . '/dac/custom-types-tax/job.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/position_type.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/resource.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/subject-tax.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/type-tax.php');
require_once(dirname(__FILE__) . '/dac/custom-types-tax/slideshow.php');
